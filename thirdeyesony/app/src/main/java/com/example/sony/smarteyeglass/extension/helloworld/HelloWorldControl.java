/*
Copyright (c) 2011, Sony Mobile Communications Inc.
Copyright (c) 2014, Sony Corporation

 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 * Neither the name of the Sony Mobile Communications Inc.
 nor the names of its contributors may be used to endorse or promote
 products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.example.sony.smarteyeglass.extension.helloworld;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sony.smarteyeglass.SmartEyeglassControl;
import com.sony.smarteyeglass.extension.util.SmartEyeglassControlUtils;
import com.sony.smarteyeglass.extension.util.SmartEyeglassEventListener;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;
import com.sonyericsson.extras.liveware.extension.util.control.ControlTouchEvent;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

/**
 * Demonstrates how to communicate between an Android activity and its
 * corresponding SmartEyeglass app.
 *
 */
public final class HelloWorldControl extends ControlExtension {

    /** Instance of the SmartEyeglass Control Utility class. */
    private final SmartEyeglassControlUtils utils;

    /** The SmartEyeglass API version that this app uses */
    private static final int SMARTEYEGLASS_API_VERSION = 1;

    /** Quality parameter for encoding PNG data. */
    private static final int PNG_QUALITY = 100;

    /** Size of initial buffer for encoding PNG data. */
    private static final int PNG_DEFAULT_CAPACITY = 256;

    /** The application context. */
    private Context context;

    /** The current power mode. */
    private int currentPowerMode;

    /** Power mode status, {@code true} if the power mode is changing. */
    private boolean isChanging = false;



    public Lepton lepton;
    Thread serverFrameThread = null;

    /** Map power mode to a descriptive display string. */
    private SparseArray<String> modeTextMap = new SparseArray<String>();

    Handler handler;
    /**
     * Shows a simple layout on the SmartEyeglass display and sets
     * the text content dynamically at startup.
     * Tap on the device controller touch pad to start the Android activity
     * for this app on the phone.
     * Tap the Android activity button to run the SmartEyeglass app.
     *
     * @param context            The context.
     * @param hostAppPackageName Package name of SmartEyeglass host application.
     */
    public HelloWorldControl(final Context context,
            final String hostAppPackageName, final String message) {
        super(context, hostAppPackageName);

        this.context = context;

        lepton = new Lepton();

        // Map power-mode constants to descriptive strings
        modeTextMap.append(SmartEyeglassControl.Intents.POWER_MODE_HIGH, "HIGH");
        modeTextMap.append(SmartEyeglassControl.Intents.POWER_MODE_NORMAL, "NORMAL");

        // Define event handler
        SmartEyeglassEventListener listener = new SmartEyeglassEventListener() {
            @Override
            // Respond to change of power mode by displaying current mode
            public void onChangePowerMode(final int powerMode) {
                currentPowerMode = powerMode;
                isChanging = false;
                String status = modeTextMap.get(powerMode);
                if (status == null) {
                    status = "UNKNOWN";
                }
//                updateScreen("Power mode is " + status);
                Log.i("POWER MODE", "Power mode is " + status);
            }
        };
        utils = new SmartEyeglassControlUtils(hostAppPackageName, listener);
        utils.setRequiredApiVersion(SMARTEYEGLASS_API_VERSION);
        utils.activate(context);
        changePowerMode(SmartEyeglassControl.Intents.POWER_MODE_HIGH);

        /*
         * Set reference back to this Control object
         * in ExtensionService class to allow access to SmartEyeglass Control
         */
        HelloWorldExtensionService.Object.SmartEyeglassControl = this;

        /*
         * Show the message that was set Iif any) when this Control started
         */
        if (message != null) {
            showToast(message);
        } else {
//            updateLayout();
        }

    }

    /**
     * Provides a public method for ExtensionService and Activity to call in
     * order to request start.
     */
    public void requestExtensionStart() {
        startRequest();
    }

    // Update the SmartEyeglass display when app becomes visible
    @Override
    public void onResume() {
//        updateLayout();
        super.onResume();
    }

    // Clean up data structures on termination.
    @Override
    public void onDestroy() {
        Log.d(Constants.LOG_TAG, "onDestroy: HelloWorldControl");
        utils.deactivate();
    };

    /**
     * Process Touch events.
     * This starts the Android Activity for the app, passing a startup message.
     */
    @Override
    public void onTouch(final ControlTouchEvent event) {
        super.onTouch(event);

        if(serverFrameThread == null) {
            serverFrameThread = new ServerFrameThread();
            serverFrameThread.start();
        }

//        HelloWorldExtensionService.Object
//                .sendMessageToActivity("Hello Activity");
    }


    class ServerFrameThread extends Thread {
        private short endFrame = 0;
        private short seqNum = 0;
        private int segment = 0;
        private int lenData = 0;
        private int offset = 0;
        private int width = 0;
        private int height = 0;

        private byte[] ByteRxBuf;
        private short[] LeptonTempArray;


        private static final int SERVER_FRAME_PORT = 5004;

        private Socket clientFrameSocket;

        private BufferedInputStream input;
        private BufferedWriter output;
        private byte[] buf;

        public ServerFrameThread() {
            buf = new byte[1400];
            ByteRxBuf = new byte[160*120*2];
            LeptonTempArray = new short[160*120];
        }

        public void run() {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    if(clientFrameSocket == null) {
                        clientFrameSocket = new Socket(InetAddress.getByName("192.168.1.1"), SERVER_FRAME_PORT);
                        input = new BufferedInputStream(clientFrameSocket.getInputStream(), 1400);
                        output = new BufferedWriter(new OutputStreamWriter(clientFrameSocket.getOutputStream()));
                    } else if ( clientFrameSocket.isClosed() ) {
                        clientFrameSocket = new Socket(InetAddress.getByName("192.168.1.1"), SERVER_FRAME_PORT);
                        input = new BufferedInputStream(clientFrameSocket.getInputStream(), 1400);
                        output = new BufferedWriter(new OutputStreamWriter(clientFrameSocket.getOutputStream()));
                    }

                    input.read(buf);

                    ByteBuffer byteBuffer = ByteBuffer.wrap(buf);

                    // Parse TCP packet
                    endFrame = byteBuffer.getShort(0);
                    offset = byteBuffer.getShort(2);
                    lenData = (int) byteBuffer.getShort(4);
                    segment = (int) byteBuffer.getShort(6);
                    width = (int) byteBuffer.getShort(8);
                    height = (int) byteBuffer.getShort(10);


                    // payload started at 12 byte
                    if( (width == 160) && (height == 120) )
                        System.arraycopy(buf, 12, ByteRxBuf, offset+(segment)*9600, lenData);
                    else if( (width == 80) && (height == 60) )
                        System.arraycopy(buf, 12, ByteRxBuf, offset, lenData);


                    // Frame has been completely received
                    if( (endFrame == 1) && (segment == 3) )
                    {
                        int d;
                        long prevTime = System.currentTimeMillis();


                        // Convert Byte[] received buff to Short[]
                        ShortBuffer shortBuf = ByteBuffer.wrap(ByteRxBuf).asShortBuffer();
                        shortBuf.get(LeptonTempArray);

                        lepton.SetImage(LeptonTempArray, width, height);
                        Canvas comboImage;


                        int outBitmapWidth = width;
                        int outBitmapHeight = height;


                        Bitmap outBitmap;
                        outBitmap = Bitmap.createBitmap(outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888);
                        comboImage = new Canvas(outBitmap);
                        comboImage.drawBitmap( Bitmap.createBitmap(lepton.toGray(), outBitmapWidth, outBitmapHeight, Bitmap.Config.ARGB_8888), 0f, 0f, null );

                        d = (int) (System.currentTimeMillis() - prevTime);
                        //Log.i("interpTime", Integer.toString(d));
                        updateBitmap(outBitmap);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {
                if(clientFrameSocket != null)
                    clientFrameSocket.close();

                //if(serverFrameSocket != null)
                //serverFrameSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     *  Update the display with the dynamic message text.
     */
    private void updateLayout() {
        showLayout(R.layout.layout, null);
        sendText(R.id.btn_update_this, "Hello World!");
    }


    /**
     * Renders a bitmap to the display.
     * Populates a layout to compose the image and text,
     * then converts it to a bitmap for display with showBitmap().
     */
    private void updateBitmap(Bitmap leptonBitmap) {
        // Initialize layout display parameters
        RelativeLayout root = new RelativeLayout(context);
        root.setLayoutParams(new RelativeLayout.LayoutParams(
                R.dimen.smarteyeglass_control_width,
                R.dimen.smarteyeglass_control_height));

        // Set dimensions and properties of the bitmap to fit the screen.
        final ScreenSize size = new ScreenSize(context);
        final int width = size.getWidth();
        final int height = size.getHeight();
        Bitmap bitmap =
                Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        bitmap.compress(Bitmap.CompressFormat.PNG, PNG_QUALITY,
                new ByteArrayOutputStream(PNG_DEFAULT_CAPACITY));
        bitmap.setDensity(DisplayMetrics.DENSITY_DEFAULT);
//
//        // Use the bitmap.xml layout resource as a base.
//        RelativeLayout layout = (RelativeLayout) RelativeLayout.inflate(context,
//                R.layout.bitmap, root);
//        // Sets dimensions of the layout to those of the bitmap.
//        layout.measure(height, width);
//        layout.layout(0, 0, layout.getMeasuredWidth(),
//                layout.getMeasuredHeight());
//
//        // Set the icon to add to the layout's ImageView element.
//        ImageView imageView = (ImageView) layout.findViewById(R.id.imageView);
//        imageView.setImageBitmap(leptonBitmap);


        Rect leptRectSrc = new Rect(0, (80*height)/width, 80, (80*height*2)/width);
        Rect leptRectDst = new Rect(0, 0, width, height);

        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        // Convert the entire layout to a bitmap using a canvas.
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(leptonBitmap, leptRectSrc, leptRectDst, paint);
        // Update the screen to display the bitmap
        utils.showBitmap(bitmap);
    }

    /**
     * Sets the power mode.
     *
     * @param mode The new power mode constant, one of:
     *            {@link SmartEyeglassControl.Intents.POWER_MODE_HIGH}, or
     *            {@link SmartEyeglassControl.Intents.POWER_MODE_NORMAL}
     */
    private void changePowerMode(final int mode) {
        isChanging = true;
//        updateScreen("Power mode changes now...");
        utils.setPowerMode(mode);
    }

    /**
     * Timeout dialog messages are similar to Toast messages on
     * Android Activities
     * This shows a timeout dialog with the specified message.
     */
    public void showToast(final String message) {
        Log.d(Constants.LOG_TAG, "Timeout Dialog : HelloWorldControl");
        utils.showDialogMessage(message,
                SmartEyeglassControl.Intents.DIALOG_MODE_TIMEOUT);
    }
}
